package com.example.g24

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    private lateinit var editTextEmail: TextInputEditText
    private lateinit var editTextPassword: TextInputEditText
    private lateinit var editTextConfirmPassword: TextInputEditText
    private lateinit var buttonRegistration: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()

        registerListener()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword)
        buttonRegistration = findViewById(R.id.buttonRegistration)
    }

    private fun registerListener() {
        buttonRegistration.setOnClickListener {

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Fill form.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password.length < 9) {
                Toast.makeText(this, "Minimum 9 symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 upper-case symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[a-b].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 lower-case symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!password.matches(".*[0-1].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 number in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (password != editTextConfirmPassword.text.toString()) {
                Toast.makeText(this, "Invalid confirm password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)

                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(this, ProfileActivity::class.java))
                        finish()
                    } else {
                        Log.d("MY_TAG", task.exception.toString())
                        Toast.makeText(this, "Registration failed.", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }

}