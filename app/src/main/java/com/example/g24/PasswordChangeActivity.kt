package com.example.g24

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {

    private lateinit var editTextPassword: TextInputEditText
    private lateinit var buttonPasswordChange: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_change)
        init()

        registerListener()

    }

    private fun init() {
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonPasswordChange = findViewById(R.id.buttonPasswordChange)
    }

    private fun registerListener() {
        buttonPasswordChange.setOnClickListener {

            val newPassword = editTextPassword.text.toString()

            if (newPassword.isEmpty()) {
                Toast.makeText(this, "Enter password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (newPassword.isEmpty()) {
                Toast.makeText(this, "Empty.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (newPassword.length < 9) {
                Toast.makeText(this, "Minimum 9 symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[A-Z].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 upper-case symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[a-b].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 lower-case symbol in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!newPassword.matches(".*[0-1].*".toRegex())) {
                Toast.makeText(this, "Minimum 1 number in password.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Password changed successfully.", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }

}